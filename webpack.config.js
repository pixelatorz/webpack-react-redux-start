const path = require('path');
const webpack = require('webpack');
const ROOT = path.resolve(__dirname, 'src');
/**
 * Webpack Plugins
 */
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
  context: ROOT,

  entry: ['./js/app.js',  './style/style.scss'],

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'js/bundle.js',
    chunkFilename: '[name].chunk.js',
    publicPath: '/'
  },

  resolve: {
    modules: ["node_modules", __dirname + "/"],
  },

  devtool: 'cheap-module-source-map',

  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "stage-1"],
        }
      },
      {
        test: /\.(scss|sass)$/,
        use: ['extracted-loader'].concat(ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use:  [{
            loader: "css-loader",
            options: {
              sourceMap: true
            }
          }, {
            loader: "sass-loader",
            options: {
              sourceMap: true,
              includePaths: [`./style/**/`],
            }
          }],
          publicPath: './'
        }))
      },
      {
        test: /\.(jpe?g|png|gif|svg)$/i,
        use: [
          'file-loader?name=[name].[ext]&outputPath=../images/&limit=1000000',
          'image-webpack-loader'
        ]
      },
      {
        test: /\.(otf|eot|ttf|woff2?)(\?\S*)?$/,
        use: 'file-loader?name=[name].[ext]&outputPath=./font/flanders/',
      },
      {
        test: /\.(otf|eot|ttf|woff2?)(\?\S*)?$/,
        use: 'file-loader?name=[name].[ext]&outputPath=./font/icon/',
      }
    ]
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('production')
      }
    }),
    new HtmlWebpackPlugin({
      title: 'Webpack',
      template: './html/index.html',
      filename: 'index.html',
      inject: true,
    }),
    new ExtractTextPlugin('css/style.css'),
    new CopyWebpackPlugin([
      {
        from: '../images/',
        to: 'images/'
      }
    ])
  ],

  watch: true,

  devServer: {
    historyApiFallback: true,
    contentBase: path.join(__dirname, "dist"),
    compress: true,
    hot: true,
    inline: true,
    port: 3000,
  }
};