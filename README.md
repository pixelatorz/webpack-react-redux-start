# Webpack - React & Redux start framework

### Getting Started

There are two methods for getting started with this repo.

```
> git clone git@bitbucket.org:pixelatorz/webpack-react-redux-start.git
> cd webpack-react-redux-start
> yarn install
> yarn build
```

